package ru.t1.semikolenov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.semikolenov.tm.api.ProjectEndpoint;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.semikolenov.tm.api.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(projectService.findAll());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        return projectService.add(project);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody final List<Project> projects
    ) {
        projectService.remove(projects);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}
