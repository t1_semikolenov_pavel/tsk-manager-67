package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    public Project add(@NotNull final String name) {
        final Project project = new Project(name);
        return projectRepository.save(project);
    }

    public Project add(@NotNull final Project project) {
        return projectRepository.save(project);
    }

    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    public boolean existsById(@NotNull final String id) {
        return projectRepository.existsById(id);
    }

    public long count() {
        return projectRepository.count();
    }

    public Project save(@NotNull final Project project) {
        return projectRepository.save(project);
    }

    public void remove(@NotNull final Project project) {
        projectRepository.delete(project);
    }

    public void removeById(@NotNull final String id) {
        projectRepository.deleteById(id);
    }

    public void remove(@NotNull final List<Project> projects) {
        projects.forEach(this::remove);
    }

    public void clear() {
        projectRepository.deleteAll();
    }

}
