package ru.t1.semikolenov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;

import java.util.List;

@NoRepositoryBean
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

    @NotNull
    List<M> findAllByUserId(@NotNull String userId);

    @NotNull
    List<M> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    M findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Transactional
    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Transactional
    void deleteAllByUserId(@NotNull String userId);

}
